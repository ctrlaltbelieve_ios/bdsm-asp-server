﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace project.Models
{
    public class Message
    {
        [Key]
        public int id { get; set; }
        public string TargetGroup { get; set; }
        public string AuthorId { get; set; }
        public string Announcement { get; set; }

    }
}