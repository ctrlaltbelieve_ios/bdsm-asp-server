﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace project.Models
{
    public class Group
    {
        [Key]
        public string GroupId { get; set; }
        public string GroupName { get; set; }
        public string Members { get; set; }

    }
}